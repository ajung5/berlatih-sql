#Soal 1 membuat database
Microsoft Windows [Version 10.0.19042.1202]
(c) Microsoft Corporation. All rights reserved.

C:\Users\Hyung Ajung>cd c:/xampp/mysql/bin

c:\xampp\mysql\bin>mysql -u root
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 21
Server version: 10.1.19-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2016, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.00 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| ajung              |
| db_borju           |
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| sg                 |
| test               |
| ujian              |
+--------------------+
10 rows in set (0.00 sec)


#Soal 2 membuat tabel
MariaDB [myshop]> CREATE TABLE users (
    -> id int NOT NULL AUTO_INCREMENT,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> PRIMARY KEY(id));
Query OK, 0 rows affected (0.02 sec)

MariaDB [myshop]> CREATE TABLE categories (
    -> id int NOT NULL AUTO_INCREMENT,
    -> name varchar(255),
    -> PRIMARY KEY(id));
Query OK, 0 rows affected (0.03 sec)

MariaDB [myshop]> CREATE TABLE items (
    -> id int NOT NULL AUTO_INCREMENT,
    -> name varchar(255),
    -> desription varchar(255),
    -> price int,
    -> stock int,
    -> PRIMARY KEY(id),
    -> category_id int,
    -> FOREIGN KEY(category_id) REFERENCES categories(id));
Query OK, 0 rows affected (0.03 sec)

MariaDB [myshop]> SHOW TABLES;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| items            |
| users            |
+------------------+
3 rows in set (0.00 sec)

MariaDB [myshop]> DESCRIBE ITEMS;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| desription  | varchar(255) | YES  |     | NULL    |                |
| price       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.02 sec)

MariaDB [myshop]> DESCRIBE CATEGORIES;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.02 sec)

MariaDB [myshop]> DESCRIBE USERS;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.01 sec)

#Soal 3 Memasukkan Data pada Table
MariaDB [myshop]> INSERT INTO users (name, email, password)
    -> VALUES
    -> ('John Doe', 'john@doe.com', 'john123'),
    -> ('Jane Doe', 'jane@doe.com', 'jenita123');
Query OK, 2 rows affected (0.00 sec)
Records: 2  Duplicates: 0  Warnings: 0

MariaDB [myshop]> SELECT * FROM USERS;
+----+----------+--------------+-----------+
| id | name     | email        | password  |
+----+----------+--------------+-----------+
|  2 | John Doe | john@doe.com | john123   |
|  3 | Jane Doe | jane@doe.com | jenita123 |
+----+----------+--------------+-----------+
2 rows in set (0.00 sec)

MariaDB [myshop]> INSERT INTO categories (name)
    -> VALUES
    -> ('gadget'),
    -> ('cloth'),
    -> ('men'),
    -> ('women'),
    -> ('branded');
Query OK, 5 rows affected (0.01 sec)
Records: 5  Duplicates: 0  Warnings: 0

MariaDB [myshop]> SELECT * FROM categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.00 sec)

MariaDB [myshop]> INSERT INTO items (name, desription, price, stock, category_id)
    -> VALUES
    -> ('Sumsang b50', 'hape keren dari merk sumsang','4000000','100','1'),
    -> ('Uniklooh', 'baju keren dari brand ternama','500000','50','2'),
    -> ('IMHO Watch', 'jam tangan anak yang jujur banget','2000000','10','1');
Query OK, 3 rows affected (0.00 sec)
Records: 3  Duplicates: 0  Warnings: 0

MariaDB [myshop]> SELECT * FROM items;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | desription                        | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang      | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.00 sec)

#Soal 4 mengambil data dari database

#a. Mengambil data users
//Buatlah sebuah query untuk mendapatkan data seluruh user pada table users.
//Sajikan semua field pada table users KECUALI password nya.

MariaDB [myshop]> SELECT DISTINCT id, name, email FROM users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  2 | John Doe | john@doe.com |
|  3 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.00 sec)

#b. Mengambil data items
//Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
MariaDB [myshop]> SELECT * FROM items WHERE price > 1000000;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | desription                        | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang      | 4000000 |   100 |           1 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
2 rows in set (0.00 sec)

//Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan
//kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
MariaDB [myshop]> SELECT * FROM items
    -> WHERE name LIKE '%Watch';
+----+------------+-----------------------------------+---------+-------+-------------+
| id | name       | desription                        | price   | stock | category_id |
+----+------------+-----------------------------------+---------+-------+-------------+
|  3 | IMHO Watch | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+------------+-----------------------------------+---------+-------+-------------+
1 row in set (0.00 sec)


#c. Menampilkan data items join dengan kategori
//Buatlah sebuah query untuk menampilkan data items yang dilengkapi dengan
//data nama kategori di masing-masing items. Berikut contoh tampilan data yang ingin didapatkan
MariaDB [myshop]> SELECT items.name, items.desription, items.price, items.stock, items.category_id, categories.name
    -> FROM items
    -> INNER JOIN categories ON items.category_id=categories.id ORDER BY stock DESC;
+-------------+-----------------------------------+---------+-------+-------------+--------+
| name        | desription                        | price   | stock | category_id | name   |
+-------------+-----------------------------------+---------+-------+-------------+--------+
| Sumsang b50 | hape keren dari merk sumsang      | 4000000 |   100 |           1 | gadget |
| Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 | cloth  |
| IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 | gadget |
+-------------+-----------------------------------+---------+-------+-------------+--------+
3 rows in set (0.00 sec)


#Soal 5 Mengubah Data dari Database
//Ubahlah data pada table items untuk item dengan
//nama sumsang b50 harganya (price) menjadi 2500000.
//Masukkan query pada text jawaban di nomor ke 5.

MariaDB [myshop]> SELECT * FROM ITEMS;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | desription                        | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang      | 4000000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.00 sec)

MariaDB [myshop]> UPDATE items
    -> SET price = 2500000
    -> WHERE name = 'Sumsang b50';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> SELECT * FROM ITEMS;
+----+-------------+-----------------------------------+---------+-------+-------------+
| id | name        | desription                        | price   | stock | category_id |
+----+-------------+-----------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | hape keren dari merk sumsang      | 2500000 |   100 |           1 |
|  2 | Uniklooh    | baju keren dari brand ternama     |  500000 |    50 |           2 |
|  3 | IMHO Watch  | jam tangan anak yang jujur banget | 2000000 |    10 |           1 |
+----+-------------+-----------------------------------+---------+-------+-------------+
3 rows in set (0.00 sec)
